{ pkgs ? import ./fetch-nixpkgs.nix {}, ... }:
with pkgs.haskell.lib;

((import ./stack2nix.nix { inherit pkgs; }).override {
  overrides = self: super: {
    # TODO: separate out output
    stack2nix = justStaticExecutables (overrideCabal super.rio-etc-template (old: {
        src = builtins.path {
            name = "rio-etc-template";
            path = ./.;
            # Filter hidden dirs (.), e.g. .git and .stack-work
            # TODO Remove once https://github.com/input-output-hk/stack2nix/issues/119 is done
            filter = path: type:
                !(pkgs.lib.hasPrefix "." (baseNameOf path));
        };
    }));
  };
}).rio-etc-template
