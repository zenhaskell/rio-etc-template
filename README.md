# rio-etc-template

A comprehensive stack project utilizing the
[RIO](https://hackage.haskell.org/package/rio) prelude and the
[etc](https://hackage.haskell.org/package/etc) configuration manager. This
builds in gitlab's ci to produce an output binary.

## Building

Build with

    stack build

Run with

    stack exec -- app-exe
